

## Types of Policies

See https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_managed-vs-inline.html

- Managed - customer or AWS 
- Inline - a policy that's embedded in a principal entity (a user, group, or role)

## IAM Tags

- https://docs.aws.amazon.com/IAM/latest/UserGuide/access_iam-tags.html 
- https://aws.amazon.com/blogs/security/add-tags-to-manage-your-aws-iam-users-and-roles
- https://aws.amazon.com/blogs/security/working-backward-from-iam-policies-and-principal-tags-to-standardized-names-and-tags-for-your-aws-resources/